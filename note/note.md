# Gitlab

## Introducción

    - Gitlab es una versión open source que cuenta con respositorios libres y gratuitos.
    - Es un servidor donde se puede hostear la app.
    - Hay una serie de pasos que hacer para llevar nuestro código a producción.
        - Dev
            - Plan
            - Create
            - Verify
            - Package
        - Ops            
            - Release
            - Configure
            - Monitor
    - Sitio Oficial
        - https://about.gitlab.com/
    - Gestion para DevOps
        - Unifica todo en un solo proceso para facilitar el proceso de deploy y post deploy.
        - https://about.gitlab.com/direction/maturity/
    - Comparación:
        - https://about.gitlab.com/devops-tools/
    - Caracteristicas:
        - https://about.gitlab.com/direction/maturity/
    - CI/CD
        - CI => Continuos Integration
        - CD => Continuos Delivery
        - La construcción de desarrollo paso de ser algo artesanal a algo mas estructurado.
            - Pasando por pruebas,
            - verificación de código, etc.
            - Podriamos tener todo un pipeline para que realize el proceso.
        - Ejemplo:
            - Upstream
                - Gitlab shell
            - Test
                - bundler.audit
                - bundler.check
                - licence_finder
                - lint-doc
                - rspec
            - Post tests
                - rake db:migrate
            - Downstrean         
                - GitlabCE
                - GitlabCI
    - Gitlab ofrece muchas caracteristicas si dejas el repositorio como publico (90%)
    - Si el proyecto es privado se tiene como el 50% de lo que es gitlab.
    - Por ahora a esta fecha gitlab es quien tiene el ciclo de integración mas completa.

# Crear DEMO.
    - Crear repositorio publica en gitlab
    - Crear proyecto:
        - ng new codelab-angular-devops --enable-ivy
            - enable-ivy es el nuevo motor de render en angular.
                - genera bundle mas pequeños.
        - Definiendo el ciclo
            - Dependencias
            - Testing
            - Build
            - Deploy
        - La cultura de DevOps va muy de la mano con la calidad, esto se segura con Pruebas unitarias.
            - Angular Tests CI
            - Se debe configurar el servidor de pruebas para el proceso de integración.
            - Documentación:
                - https://angular.io/guide/build
            - Entrar al archivo de karma y configurar:
                customLaunchers: {
                    ChromeHeadlessCI: {
                        base: 'ChromeHeadless',
                        flags: ['--no-sandbox']
                    }
                }
        - Generar scripts para correr pruebas
            - ng test
                --no-watch
                --code-coverage
                --browsers=ChromeHeadlessCI
            - Con esto podemos correr un comando configurado como npm run test:ci
        - Posteriormente necesitamos crear el archivo que se en cargar de realizar continuos integration
            - .gitlab-ci.yml
                - Este archivo iniciara con una imagen que validara las correspondientes jobs
                - Primer paso:
                    - Agregar la imagen a utilizar y los pasos de instalación.
                    - Comenzar con el paso de instalación
                    - Comenzar con el paso de test
                        - Toda esta configuración se puede realizar con una imagen de docker que ya se encuentra todo configurado con google chrome.
                        - Se crea una expresión regular que obtenga el valor de porcentaje de cobertura.
                        - Es recomendable pasar por un validador nuestra configuración
                            - gitlab en el apartado de CI/CD y posterior en pipelines trae una opción de CI LINT donde podemos validar la congiguración yml
                            - Documentación para la revisión del porcentaje de cobertura:
                                - https://angular.io/guide/testing-code-coverage
                        - Al relizar el MR tal cual como se tiene ahora el pipeline tiene configurado para que realice install y test
                            - esto lo hare al subir la rama y tambien al integrarse a master
                            - Sin embargo cuado realicemos los pasos de build y deploy se agrega funcionalidad que eso lo haga solo cuando se integre a master.                                
                    - Comenzar con el paso de build
                        - Se va a usar gitlab pages
                            - https://about.gitlab.com/stages-devops-lifecycle/pages/
                                - Recordemos que la url quedara de acuerdo como tengas la cuenta en gitlab
                    - Comenzar con el paso de deploy
                        - Se usara gitlab pages.

# GitLab Runner
    - Es una opción de gitlab proporciona para usar los servidores propios y no usar los servidores compartidos.
        - Unir servidor propio al ciclo de gitlab
        - Esto hace que todo el ciclo de pipeline puede correr en los servidores propios y no los compartidos.
    - Si tienes repositorio privado, este uso de los servidores compartidos tiene un tiempo limito.
    - Configuración de Runner 
        - https://gitlab.com/shovanny/codelab-angular/-/settings/ci_cd    
        - Es recomendable usar digital ocean por su simpleza
            - otras opciones:
                - azure
                - aws             

# Crear issues en Gitlab y en base a el generar ramas
    - Sección Issues
    - Posterior crear issue
        - Title
        - en descripción se le puede colocar
            /terminate 2h
    -            
# Comandos Git
    - shorcut para git checkout -b nameBranch
        - gcb nameBranch
    - shortcut para agregar git add .
        - gaa
    - shortcul para git commit
        - gc
    - shorcut para git push
        - ggp   
    - shorcut git status
        - gst  
    - shorcut git checkout master                                
        - gco master
    - shortcut git pull    
        - ggl                                    
